#!/usr/bin/env python3

# Rutinas para codificacion de
# longitud variable de enteros

def vl_decode(stream, n=-1):
    while n != 0:
        value = 0
        shift = 0
        try:
            x = stream.read(1)[0]
        except IndexError:
            return # eof
        while x >= 128:
            value += (x & 127) << shift
            shift += 7
            x = stream.read(1)[0]
        value += x << shift
        n -= 1
        yield value

def vl_encode(stream, values):
    for value in values:
        while value >= 1<<7:
            stream.write(bytes([(value & 127) | 128]))
            value = value >> 7
        stream.write(bytes([value & 127]))

