#!/usr/bin/env python3
import sys
import os
import csv
import argparse
from collections import OrderedDict
from vle import vl_encode, vl_decode
import re

# Implementación del algoritmo Lempel-Ziv-Welch
# para la compresión de genomas.
# Genera un diccionario en formato csv con:
# Patrón, Repeticiones, Porcentaje representado
# @author Altoe, Francisco; Juran, Martín Tomás
# @version 1.1, 2018-10-06

BASES = (
    #'R', 'Y', 'K', 'M', 'S', 'W', # Consultar https://en.wikipedia.org/wiki/FASTA_format
    #'B', 'D', 'H', 'V', 'N',      # Para estudiar complejidad no se utilizan
    'A', 'C', 'G', 'T', 'U'
)

class DictionaryEntry:
    def __init__(self, entryid):
        self.posiciones = []
        self.entryid = entryid # token de reemplazo

def cambiar_extension(path, nueva_extension):
    return re.sub('.[^.]+$', nueva_extension, path)

def descomprimir(comprimido_path, diccionario, long_linea=70):
    diccionario = OrderedDict((b, DictionaryEntry(entryid)) for entryid, b in enumerate(diccionario))
    diccionario_keys = list(diccionario.keys())
    print("Descomprimiendo \"" + comprimido_path + "\"...")
    descomprimido_path = cambiar_extension(comprimido_path, '.txt')

    with open(descomprimido_path, "w") as descomprimido, open(comprimido_path, "rb") as genoma:
        string = ""
        linea_salida = ""
        nlinea = 1
        for tokenid in vl_decode(genoma):
            try:
                token = diccionario_keys[tokenid]
            except IndexError as e:
                token = string + string[0]

            linea_salida += token
            while len(linea_salida) >= long_linea:
                descomprimido.write(linea_salida[0:long_linea] + "\n")
                linea_salida = linea_salida[long_linea:]
                nlinea += 1

            string_mas_base = string + token[0]
            if string_mas_base not in diccionario:
                diccionario[string_mas_base] = DictionaryEntry(len(diccionario))
                diccionario_keys.append(string_mas_base)
            else:
                diccionario[string_mas_base].posiciones.append((nlinea, len(linea_salida)+1))
            string = token
        descomprimido.write(linea_salida)
        print("Archivo descomprimido guardado en \"" + descomprimido_path + "\"")

def comprimir(gen_path, diccionario):
    """ Función principal para comprimir el archivo """
    diccionario = OrderedDict((b, DictionaryEntry(entryid)) for entryid, b in enumerate(diccionario))
    string = ""
    lin_ini = 1 # linea donde empieza el patron
    col_ini = 1 # columna donde empieza el patron
    print("Comprimiendo \"" + gen_path + "\"...")

    comprimido_path = cambiar_extension(gen_path, '.dat')
    diccionario_path = cambiar_extension(gen_path, '_dic.csv')
    with open(comprimido_path, "wb") as comprimido, open(gen_path, "r") as genoma:
        try:
            # Algoritmo de compresión LZW
            for nlinea, linea in enumerate(genoma, start=1):
                linea = linea[0:-1] # remover fin de linea

                if (nlinea % 100000 == 0):
                	print("Progreso: línea " + str(nlinea))

                # Ignorar lineas de metadatos
                if linea.startswith(">") or linea.startswith(";"):
                    continue

                # Leer letra por letra
                for ncolumna, base in enumerate(linea, start=1):
                    # Ignorar las que no son bases
                    if base not in BASES:
                        continue

                    string_mas_base = string + base

                    if string_mas_base in diccionario:
                        # Continuar
                        string = string_mas_base
                    else:
                        # nuevo patron
                        diccionario[string].posiciones.append((lin_ini, col_ini))

                        # agregar el token de reemplazo en el comprimido
                        vl_encode(comprimido, [diccionario[string].entryid])

                        # agregar el nuevo patron al diccionario
                        diccionario[string_mas_base] = DictionaryEntry(len(diccionario))
                        lin_ini, col_ini = nlinea, ncolumna
                        string = base
            # guardar el resto
            if string:
                vl_encode(comprimido, [diccionario[string].entryid])
            print("Archivo comprimido guardado en \"" + comprimido_path + "\"")
            # guardar diccionario si se termino con exito
            guardarDiccionario(diccionario, diccionario_path)
            print("Razón de compresión: " + str( # Qué porcentaje se pudo comprimir
                round((1 - (os.path.getsize(comprimido_path) / os.path.getsize(gen_path))) * 100, 2)
            ) + "%")
        except:
            raise Exception("Leyendo {}:{}".format(nlinea, ncolumna))


def cargarDiccionario(path_entrada):
    """ Carga los patrones del diccionario en una lista """
    with open(path_entrada, "r") as d:
        dicreader = csv.reader(d)
        it = iter(dicreader)
        next(it) # ignorar cabeceras
        return [entrystr for identificador, entrystr, entryfreq, posstr in it]

def guardarDiccionario(diccionario, path_salida):
    """ Guarda el diccionario en formato .csv """
    with open(path_salida, "w") as d:
        dicwriter = csv.writer(d)
        dicwriter.writerow(["ID", "PATRON", "FRECUENCIA", "POSICIONES"])
        for i, (k, v) in enumerate(diccionario.items()):
            dicwriter.writerow([i, k, len(v.posiciones), ';'.join(':'.join(map(str, p)) for p in v.posiciones)])
        print("Diccionario guardado en \"" + path_salida + "\"")

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("archivo_entrada", help="Archivo a comprimir o descomprimir")
    parser.add_argument("--diccionario", help="Diccionario predefinido")
    parser.add_argument("-d", "--descomprimir", action="store_true", help="Descomprimir en lugar de comprimir", default=False)
    args = parser.parse_args()

    if args.diccionario is not None:
        diccionario = cargarDiccionario(args.diccionario)
        print("Diccionario cargado \"" + args.diccionario + "\"")
    else:
        diccionario = BASES

    if args.descomprimir:
        descomprimir(args.archivo_entrada, diccionario)
    else:
        comprimir(args.archivo_entrada, diccionario)

