files="https://ftp.ncbi.nlm.nih.gov/genomes/M_musculus/CHR_01/mm_ref_GRCm38.p4_chr1.fa.gz \
https://ftp.ncbi.nlm.nih.gov/genomes/M_musculus/RNA/rna.fa.gz"

output_dir=$(dirname $0)
for f in $files
do
	n="$output_dir/$(basename $f)"
	if [ ! -e "$n" ]
	then
		if wget -nv --show-progress $f -O "$n"
		then
			if gunzip "$n" --verbose --keep
			then
				echo "Descargado $f"
			else
				echo "Error descomprimiendo $n"
			fi
		else
			echo "Error descargando $f"
		fi
	else
		echo "Secuencia $f encontrada"
	fi
done
