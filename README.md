# genomas-pufo

Proyecto de Investigación Biología - Sistemas de la Universidad Nacional de Luján. Análisis de complejidad en genomas con algoritmos de compresión de texto y otras técnicas.

# Instrucciones

Compresion sin diccionario predefinido:
```sh
$ python3 lzw.py genoma-prueba.fa
```
"genoma-prueba.fa" es una versión reducida del cromosoma 2 de Drosophila arizonae, cuyo archivo completo se encuentra en "7263\_ref\_ASM165402v1\_chr2.fa".

Esto va a crear dos archivos: genoma-prueba\_dic.csv, que contiene los patrones, sus frecuencias y posiciones, y el archivo genoma-prueba.dat que muestra el resultado de la compresión para comparar con el original.

Compresion con diccionario predefinido:
```sh
$ python3 lzw.py genoma-prueba.fa --diccionario genoma-prueba_dic.csv
```
En este caso la compresión se realiza en base a un diccionario precalculado.


Descompresion:
```sh
$ python3 lzw.py -d comprimido.txt
```
En este caso se descomprime el archivo comprimido generado previamente (no es necesario indicar diccionario). La salida se escribe en descomprimido.txt.


