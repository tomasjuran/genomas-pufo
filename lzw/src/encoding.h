#ifndef _LZW_ENCODING_H_
#define _LZW_ENCODING_H_

#include <iostream>

template<typename T, typename StreamT>
class VleEncoder {
public:
	VleEncoder(StreamT&& stream) : stream(std::forward<StreamT>(stream)) {
	}
	~VleEncoder() {
		flush();
	}
	VleEncoder& operator<<(T value) {
		return put(value);
	}
	VleEncoder& put(T value) {
		T v = value & 0b01111111;
		while ((value >>= 7) != 0) {
			stream.put(v | 0b10000000);
			v = value & 0b01111111;
		}
		stream.put(v);
		return *this;
	}
	VleEncoder& flush() {
		stream.flush();
		return *this;
	}
	operator bool() const { return not stream.fail(); }
	bool fail() const { return stream.fail(); }
private:
	StreamT stream;
};

template<typename T, typename StreamT>
class VleDecoder {
public:
	VleDecoder(StreamT&& stream) : stream(std::forward<StreamT>(stream)) {
	}
	T get() {
		T ret;
		get(ret);
		return ret;
	}
	VleDecoder& operator>>(T& value) {
		return get(value);
	}
	VleDecoder& get(T& value) {
		value = 0;
		int shift = 0;
		T byte;
		while ((byte = stream.get()) & 0b10000000) {
			if (not stream)
				break;
			value += (byte & 0b01111111) << shift;
			shift += 7;
		}
		value += byte << shift;
		return *this;
	}
	operator bool() const { return not stream.fail(); }
	bool fail() const { return stream.fail(); }
private:
	StreamT stream;
};

template<typename T, typename StreamT>
VleEncoder<T, StreamT> vle_encoded(StreamT&& stream) {
	return {std::forward<StreamT>(stream)};
}

template<typename T, typename StreamT>
VleDecoder<T, StreamT> vle_decoded(StreamT&& stream) {
	return {std::forward<StreamT>(stream)};
}

// TODO: Huffman? Dgaps?

#endif

