#ifndef _LZW_TRIE_H_
#define _LZW_TRIE_H_

#include <algorithm>
#include <array>
#include <memory>
#include <functional>
#include <list>

template<typename K, typename V, size_t AlphabetSize>
class Trie {
public:
	using value_type = V;
	using reference = V&;
	using const_reference = const V&;

	class iterator {
	public:
		iterator(Trie& trie) : trie(trie) {
		}
		template<typename It1, typename It2>
		iterator(Trie& trie, It1 it1, It2 it2) : trie(trie) {
			for(; it1 != it2; ++it1) {
				auto st = trie.subtrie();
				for (auto& ki : *it1) {
					st = st.subtrie(ki);
					if (st.empty())
						break;
				}
				if (st.empty())
					continue;
				queue.push_back(*it1);
			}
		}
		iterator& operator++() {
			for (auto& ki : trie.alphabet) {
				K copy = queue.front();
				copy.push_back(ki);
				if (trie.subtrie(copy).empty())
					continue;
				queue.push_back(std::move(copy));
			}
			queue.pop_front();
			return *this;
		}
		std::pair<K, V> operator*() const {
			return {queue.front(), trie[queue.front()]};
		}
		bool operator!=(const iterator& other) const {
			if (queue.size() != other.queue.size())
				return true;
			auto it1 = std::begin(queue);
			auto it2 = std::begin(other.queue);
			for (; it1 != std::end(queue); ++it1, ++it2) {
				if (*it1 != *it2)
					return true;
			}
			return false;
		}
	private:
		Trie& trie;
		std::list<K> queue;
	};

	// nodo = array de hijos + dato
	class TrieNode {
	public:
		std::array<std::unique_ptr<TrieNode>, AlphabetSize> children;
		V data;
	};

	// subtrie = trie padre + raiz subrogada
	class SubTrie {
	public:
		SubTrie(std::unique_ptr<TrieNode>& r, Trie& t) : root(r), trie(t) {}

		reference operator[](const K& key) {
			return trie.get().get_data(std::begin(key), std::end(key), root.get());
		}
		reference operator[](const typename K::value_type& ki) {
			return trie.get().get_data(ki, root.get());
		}
		reference data() {
			return trie.get().get_data(root.get());
		}
		SubTrie subtrie(const K& key) {
			return {trie.get().seek(key, root.get()), trie.get()};
		}
		SubTrie subtrie(const typename K::value_type& ki) {
			return {trie.get().seek(ki, root.get()), trie.get()};
		}
		bool empty() const {
			return not bool(root.get());
		}
	private:
		std::reference_wrapper<std::unique_ptr<TrieNode>> root;
		std::reference_wrapper<Trie> trie;
	};

	Trie(const std::array<typename K::value_type, AlphabetSize>& alphabet) : alphabet{alphabet} {
	}
	Trie() = delete; // no se puede inicializar un trie sin alfabeto

	reference operator[](const K& key) {
		return get_data(std::begin(key), std::end(key), root);
	}
	reference operator[](const typename K::value_type& ki) {
		return get_data(ki, root);
	}
	reference data() {
		return get_data(root);
	}

	SubTrie subtrie(const K& key) {
		return {seek(std::begin(key), std::end(key), root), *this};
	}
	SubTrie subtrie(const typename K::value_type& ki) {
		return {seek(ki, root), this};
	}
	SubTrie subtrie() {
		return {root, *this};
	}

	iterator begin() {
		std::list<K> l;
		for (auto& ki : alphabet) {
			K k;
			k.push_back(ki);
			l.push_back(k);
		}
		return iterator(*this, std::begin(l), std::end(l));
	}
	iterator end() {
		return iterator(*this);
	}

	size_t size() const {
		return num_nodes;
	}
	bool empty() const {
		return root;
	}
	friend class SubTrie;
	friend class iterator;
protected:
	// obtener una referencia al valor del nodo de la cadena `key`
	reference get_data(typename K::const_iterator it0, typename K::const_iterator itn, std::unique_ptr<TrieNode>& p) {
		std::unique_ptr<TrieNode>& q = seek(it0, itn, p);
		return get_data(q);
	}
	// obtener la referencia al valor asociado al subnodo del siguiente `ki`
	reference get_data(const typename K::value_type& ki, std::unique_ptr<TrieNode>& p) {
		std::unique_ptr<TrieNode>& q = seek(ki, p);
		return get_data(q);
	}
	// obtener la referencia al valor asociado al nodo `p`
	reference get_data(std::unique_ptr<TrieNode>& p) {
		if (not p) {
			p = std::make_unique<TrieNode>();
			++num_nodes;
		}
		return p->data;
	}

	// obtener subnodo que empieza donde termina la cadena `key`
	std::unique_ptr<TrieNode>& seek(typename K::const_iterator it0, typename K::const_iterator itn, std::unique_ptr<TrieNode>& p) {
		if (it0 != itn) {
			std::unique_ptr<TrieNode>& q = seek(*it0, p);
			return seek(++it0, itn, q);
		}
		return p;
	}

	// obtener el subnodo del siguiente `ki`
	std::unique_ptr<TrieNode>& seek(const typename K::value_type& ki, std::unique_ptr<TrieNode>& p) {
		if (not p) {
			p = std::make_unique<TrieNode>();
			++num_nodes;
		}
		auto it = std::lower_bound(std::begin(alphabet), std::end(alphabet), ki);
		return p->children[it-std::begin(alphabet)];
	}
private:
	std::array<typename K::value_type, AlphabetSize> alphabet;
	std::unique_ptr<TrieNode> root;
	size_t num_nodes = 0;
};

template<typename K, typename V, size_t AlphabetSize>
Trie<K, V, AlphabetSize> make_trie(const std::array<typename K::value_type, AlphabetSize>& alphabet) {
	return {alphabet};
}


#endif
