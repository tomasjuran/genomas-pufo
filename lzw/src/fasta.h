#ifndef _LZW_FASTA_H_
#define _LZW_FASTA_H_

#include <iostream>

template<typename StreamT>
class FastaReader {
public:
	FastaReader(StreamT&& stream) : stream(std::forward<StreamT>(stream)) {
	}
	FastaReader(const FastaReader&) = delete;
	char get() {
		char ret;
		get(ret);
		return ret;
	}
	FastaReader& operator>>(char& value) {
		get(value);
		return *this;
	}
	FastaReader& get(char& value) {
		while(true) {
			value = stream.get();
			if (not fail() and (value == ';' or value == '>')) {
				while (not fail() and value != '\n') {
					value = stream.get();
				}
			}
			if (value != '\n')
				break;
		}
		return *this;
	}
	operator bool() const { return not stream.fail(); }
	bool fail() const { return stream.fail(); }
private:
	int line = 0;
	StreamT stream;
};

template<typename StreamT>
class FastaWriter {
public:
	FastaWriter(StreamT&& stream, size_t columns) : stream(std::forward<StreamT>(stream)), max_columns(columns) {
	}
	FastaWriter(const FastaWriter&) = delete;
	FastaWriter& put(const std::string& str) {
		for (char ch : str) {
			put(ch);
		}
		return *this;
	}
	FastaWriter& put(char ch) {
		if (++current_column < max_columns) {
			stream << ch;
		}
		else {
			stream << ch << "\n";
			current_column = 0;
		}
		return *this;
	}
	FastaWriter& operator<<(const std::string& str) {
		put(str);
		return *this;
	}
	FastaWriter& operator<<(char ch) {
		put(ch);
		return *this;
	}
	FastaWriter& flush() {
		stream.flush();
		return *this;
	}
	operator bool() const { return not stream.fail(); }
	bool fail() const { return stream.fail(); }
private:
	StreamT stream;
	size_t current_column = 0;
	size_t max_columns;
};

template<typename StreamT>
FastaReader<StreamT> make_fasta_reader(StreamT&& stream) {
	return FastaReader<StreamT>(std::forward<StreamT>(stream));
}

template<typename StreamT>
FastaWriter<StreamT> make_fasta_writer(StreamT&& stream, int w) {
	return FastaWriter<StreamT>(std::forward<StreamT>(stream), w);
}

#endif
