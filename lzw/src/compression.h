#ifndef _LZW_COMPRESSION_H_
#define _LZW_COMPRESSION_H_

#include <iostream>
#include <vector>
#include "trie.h"

class DictionaryEntry {
public:
	//std::vector<int> offsets = {};
	size_t frequency = 0;
	int entry_id = 0;
};

template<typename KeyT, size_t AlphabetSize, typename StreamT>
class LzwCompressor {
public:
	LzwCompressor(StreamT&& stream, std::array<typename KeyT::value_type, AlphabetSize> alphabet, const std::vector<KeyT>& dict)
	: stream(std::forward<StreamT>(stream)), dictionary(alphabet), subtrie(dictionary.subtrie()) {
		for (const auto& entry : dict) {
			dictionary[entry].entry_id = entry_id_counter++;
		}
	}
	~LzwCompressor() {
		flush();
	}
	LzwCompressor& operator<<(typename KeyT::value_type ch) {
		return put(ch);
	}
	LzwCompressor& put(typename KeyT::value_type ch) {
		// asumimos que el caracter de entrada siempre es parte del alfabeto
		// deberia ser verificado antes de llamar a este metodo
		auto subtrie2 = subtrie.subtrie(ch);
		if (not subtrie2.empty()) {
			// avanzar
			subtrie = subtrie2;
		}
		else {
			// agregar aparicion en la nueva entrada
			//subtrie2.data() = {{entry_start_offset}, entry_id_counter++};
			subtrie2.data() = {1, entry_id_counter++};
			flush();
			subtrie = subtrie.subtrie(ch);
		}
		++current_offset_counter;
		return *this;
	}
	LzwCompressor& flush() {
		if (current_offset_counter >= entry_start_offset) {
			// agregar aparicion en entrada preexistente
			//subtrie.data().offsets.push_back(entry_start_offset);
			++subtrie.data().frequency;

			// escribir al stream
			stream << subtrie.data().entry_id;

			// reiniciar el contador de inicio
			entry_start_offset = current_offset_counter + 1;

			// reiniciar el subarbol hasta la raiz
			subtrie = dictionary.subtrie();

			stream.flush();
		}
		return *this;
	}
	operator bool() const {
		return not stream.fail();
	}
	bool fail() const {
		return stream.fail();
	}

	Trie<KeyT, DictionaryEntry, AlphabetSize>& getDictionary() {
		return dictionary;
	}
private:
	StreamT stream;
	Trie<KeyT, DictionaryEntry, AlphabetSize> dictionary;
	typename Trie<KeyT, DictionaryEntry, AlphabetSize>::SubTrie subtrie;
	int entry_start_offset;
	int entry_id_counter = 0;
	int current_offset_counter = 0;
};

template<typename KeyT, size_t AlphabetSize, typename StreamT>
class LzwDecompressor {
public:
	LzwDecompressor(StreamT&& stream, std::array<typename KeyT::value_type, AlphabetSize> alphabet, const std::vector<KeyT>& dict)
	: stream(std::forward<StreamT>(stream)) {
		// array de elementos a vector de cadenas
		dictionary.reserve(dict.size());
		for (const auto& entry : dict) {
			dictionary.push_back(entry);
		}
	}
	LzwDecompressor& operator>>(KeyT& k) {
		return get(k);
	}
	KeyT get() {
		KeyT ret;
		get(ret);
		return ret;
	}
	LzwDecompressor& get(KeyT& k) {
		int token_id;
		stream >> token_id;
		if (stream.fail())
			return *this;

		KeyT token;
		if (token_id < dictionary.size()) {
			token = dictionary[token_id];
		}
		else {
			token = string;
			token.push_back(string.front());
		}

		if (string.size() > 0) {
			string.push_back(token.front());
			dictionary.push_back(string);
		}

		string = token;
		k = std::move(token);

		return *this;
	}
	operator bool() const { return not stream.fail(); }
	bool fail() const { return stream.fail(); }

private:
	KeyT string;
	StreamT stream;
	std::vector<KeyT> dictionary;
};

template<typename KeyT, size_t AlphabetSize, typename StreamT>
LzwCompressor<KeyT, AlphabetSize, StreamT> lzw_compressed(
		StreamT&& stream,
		std::array<typename KeyT::value_type, AlphabetSize> alphabet,
		const std::vector<KeyT>& dictionary
) {
	return {std::forward<StreamT>(stream), alphabet, dictionary};
}
template<typename KeyT, size_t AlphabetSize, typename StreamT>
LzwDecompressor<KeyT, AlphabetSize, StreamT> lzw_decompressed(
		StreamT&& stream,
		std::array<typename KeyT::value_type, AlphabetSize> alphabet,
		const std::vector<KeyT>& dictionary
) {
	return {std::forward<StreamT>(stream), alphabet, dictionary};
}

#endif

