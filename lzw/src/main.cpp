#include <iostream>
#include <sstream>
#include <fstream>
#include <boost/program_options.hpp>
#include "encoding.h"
#include "trie.h"
#include "compression.h"
#include "fasta.h"

namespace po = boost::program_options;

void usage(std::ostream& stream, po::options_description desc_options, char* argv0) {
	stream << "Uso: " << argv0 << " input-file" << std::endl;
	stream << desc_options << std::endl;
}

std::string change_extension(const std::string& path_in, const std::string& ext_out) {
	size_t pos = path_in.rfind('.');
	if (pos == std::string::npos)
		return "";
	else
		return path_in.substr(0, pos) + ext_out;
}

int main(int argc, char** argv) {
	po::options_description desc_options;
	desc_options.add_options()
		("help", "mostrar ayuda")
		("input-file", po::value<std::string>(), "archivo de entrada")
		("decompress,d", "indica que se desea descomprimir (en lugar de comprimir)")
		("dictionary,D", po::value<std::string>(), "diccionario de entrada")
		("fasta-width,w", po::value<int>()->default_value(70), "cantidad de columnas en el fasta de salida")
		;

	po::positional_options_description p;
	p.add("input-file", 1);
	po::variables_map vm_options;
	po::store(po::command_line_parser(argc, argv).options(desc_options).positional(p).run(), vm_options);
	po::notify(vm_options);

	if (vm_options.count("help")) {
		usage(std::cout, desc_options, argv[0]);
		return 0;
	}
	if (!vm_options.count("input-file")) {
		usage(std::cerr, desc_options, argv[0]);
		return 1;
	}

	// TODO: quitar Ns
	const std::array<char, 5> BASES {'A', 'C', 'G', 'N', 'T'};
	const std::vector<std::string> DICCIONARIO {"A", "C", "G", "T", "N"}; // debe incluir por lo menos al alfabeto (BASES)

	std::string input_file_path = vm_options["input-file"].as<std::string>();
	std::fstream input_file_stream(input_file_path, std::ios_base::in|std::ios_base::binary);

	if (input_file_stream.fail()) {
		std::cerr << "No se pudo leer el archivo de entrada " << input_file_path << std::endl;
		return 1;
	}

	if (!vm_options.count("decompress")) {
		FastaReader<decltype(input_file_stream)&> reader(input_file_stream);

		std::string output_file_path = change_extension(input_file_path, ".dat");
		std::fstream output_file_stream(output_file_path, std::ios_base::out|std::ios_base::binary);
		VleEncoder<int, decltype(output_file_stream)&> writer0(output_file_stream);
		LzwCompressor<std::string, 5, decltype(writer0)&> writer(writer0, BASES, DICCIONARIO);

		if (writer.fail()) {
			std::cerr << "No se pudo abrir el archivo de salida " << output_file_path << std::endl;
			return 1;
		}

		for (char ch = reader.get(); reader; ch = reader.get()) {
			// TODO: Filtrar Ns
			writer << ch;
		}
		writer.flush();

		input_file_stream.clear(); // limpiar flags para poder leer la posicion del archivo de entrada
		size_t size_in = input_file_stream.tellg();
		size_t size_out = output_file_stream.tellg();

		std::cout << "Exito. Archivo comprimido guardado en " << output_file_path << std::endl;
		std::cout << "Tasa de compresión: " << size_in << ":" << size_out <<
			" (" << (100. - static_cast<double>(size_out)/static_cast<double>(size_in)*100.) << "% de ahorro)" << std::endl;

		std::string dict_file_path = change_extension(input_file_path, "_dic.csv");
		std::fstream dict_file_stream(dict_file_path, std::ios_base::out);
		for (std::pair<std::string, DictionaryEntry> p : writer.getDictionary()) {
			dict_file_stream << p.second.entry_id << "," << p.first << "," << p.second.frequency << "\n";
		}
		std::cout << "Diccionario guardado en " << dict_file_path << std::endl;
	}
	else {
		VleEncoder<int, decltype(input_file_stream)&> reader0(input_file_stream);
		LzwDecompressor<std::string, 5, decltype(reader0)&> reader(reader0, BASES, DICCIONARIO);

		std::string output_file_path = change_extension(input_file_path, ".fasta");
		std::fstream output_file_stream(output_file_path, std::ios_base::out|std::ios_base::binary);
		FastaWriter<decltype(output_file_stream)&> writer(output_file_stream, vm_options["fasta_width"].as<int>());

		if (writer.fail()) {
			std::cerr << "No se pudo abrir el archivo de salida " << output_file_path << std::endl;
			return 1;
		}

		for (std::string s = reader.get(); reader; s = reader.get()) {
			writer << s;
		}
		writer.flush();

		input_file_stream.clear(); // limpiar flags para poder leer la posicion del archivo de entrada
		size_t size_in = input_file_stream.tellg();
		size_t size_out = output_file_stream.tellg();
		std::cout << "Exito. Archivo descomprimido guardado en " << output_file_path << std::endl;
		std::cout << "Tasa de compresión: " << size_out << ":" << size_in <<
			" (" << (100. - static_cast<double>(size_in)/static_cast<double>(size_out)*100.) << "% de ahorro)" << std::endl;
	}
}
