## Compilacion

Compilar con gcc:
```sh
$ g++ -std=c++17 -lboost_program_options main.cpp -o main
```

O bien, compilar con clang:
```sh
$ clang++ -std=c++17 -lboost_program_options main.cpp -o main
```

## Uso

Compresión:
```sh
./main path/al/archivo.fasta
```

Descompresión:
```sh
./main -d path/al/archivo.dat
```

## Requisitos
Se debe instalar:
 * Compilador con soporte para C++17 (gcc o clang)
 * Librerias boost
