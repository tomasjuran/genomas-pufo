
class Trie:
    '''Trie implementation'''
    def __init__(self, alphabet, data=None):
        self.alphabet = alphabet
        self.children = [None] * len(alphabet)
        self.data = None
        if data is not None:
            for k, v in data:
                self[k] = v

    def __str__(self):
        return '{} ({})'.format(
            str(self.data),
            ','.join(
                '{}: {}'.format(repr(i), str(j)) for i, j in zip(self.alphabet, self.children)
            )
        )

    def __contains__(self, key):
        try:
            self[key]
            return True
        except KeyError:
            return False

    def seek(self, key):
        '''busca la clave en el trie y devuelve el nodo del subarbol'''
        i = self
        for k in key:
            # asumimos que el alfabeto es pequeño
            i = i.children[self.alphabet.index(k)]
            if i is None:
                raise KeyError(key)
        if i.data is None:
            raise KeyError(key)
        return i

    def __getitem__(self, key):
        return self.seek(key).data

    def __setitem__(self, key, data):
        i = self
        for k in key:
            j = self.alphabet.index(k)
            if i.children[j] is None:
                # crear un hijo vacio que enlace al siguiente
                i.children[j] = Trie(self.alphabet)
            i = i.children[j]
        i.data = data

    def __delitem__(self, key):
        self.seek(key).data = None
        # TODO: purgar hojas?

    def __iter__(self):
        if self.data is not None:
            yield self.data
        for i in self.children:
            if i is not None:
                for j in i:
                    yield j

